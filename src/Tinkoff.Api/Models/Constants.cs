﻿namespace Tinkoff.Api.Models
{
    public class Constants
    {
        public const double DAYS_PER_MONTH = 30.4375;
        public const double DAYS_PER_YEAR = 365.25;
        public const int PENSION_AGE = 60;
    }
}