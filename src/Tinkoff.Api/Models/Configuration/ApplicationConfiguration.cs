﻿using System;

namespace Tinkoff.Api.Models.Configuration
{
    public class ApplicationConfiguration
    {
        public Guid CreditOrganizationId { get; set; }
    }
}