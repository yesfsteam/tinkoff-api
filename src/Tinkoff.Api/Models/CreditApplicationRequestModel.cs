﻿using System;
using System.Net;
using Yes.Infrastructure.Common.Models;

namespace Tinkoff.Api.Models
{
    public class CreditApplicationRequestModel : JsonModel
    {
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Http статус запроса
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// Код ошибки
        /// </summary>
        public string TinkoffResultCode { get; set; }
        
        /// <summary>
        /// Идентификатор заявки Tinkoff
        /// </summary>
        public string TinkoffPayloadId { get; set; }
        
        /// <summary>
        /// Массив ошибок, в формате Json
        /// </summary>
        public string TinkoffPayloadErrors { get; set; }
        
        /// <summary>
        /// TrackingId
        /// </summary>
        public string TinkoffTrackingId { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string TinkoffErrorMessage { get; set; }
        
        /// <summary>
        /// Описание ошибки
        /// </summary>
        public string TinkoffPlainMessage { get; set; }
    }
}