﻿using Yes.Infrastructure.Common.Models;

namespace Tinkoff.Api.Models
{
    public class TinkoffCreditApplicationResponse : JsonModel
    {
        public string resultCode { get; set; }
        public TinkoffPayload payload { get; set; }
        public string trackingId { get; set; }
        
        //todo bad request
        public string errorMessage { get; set; }
        public string plainMessage { get; set; }
    }
}