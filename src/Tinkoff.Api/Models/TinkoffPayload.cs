﻿using System.Collections.Generic;

namespace Tinkoff.Api.Models
{
    public class TinkoffPayload
    {
        public string id { get; set; }
        public List<string> errors { get; set; }
    }
}