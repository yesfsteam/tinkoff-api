﻿﻿ namespace Tinkoff.Api.Models.Enums
{
    public enum TinkoffMaritalStatus
    {
        Single = 1,
        Divorced = 2,
        CivilMarriage = 3,
        Married = 4,
        Widowed = 5
    }
}