﻿﻿ namespace Tinkoff.Api.Models.Enums
{
    public enum TinkoffPhoneNumberType
    {
        My = 0,
        Relative = 1,
        Friend = 2
    }
}