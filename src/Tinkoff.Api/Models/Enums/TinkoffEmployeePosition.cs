﻿﻿  namespace Tinkoff.Api.Models.Enums
{
    public enum TinkoffEmployeePosition
    {
        None = 0,
        Head = 1,
        DivisionHead = 2,
        Specialist = 3,
        ServiceStaff = 4,
        Employee = 6
    }
}