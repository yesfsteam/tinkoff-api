﻿﻿ using System.ComponentModel;

  namespace Tinkoff.Api.Models.Enums
{
    public enum TinkoffActivity
    {
        [Description("work")]
        Work,
        [Description("businessman")]
        Businessman,
        [Description("not_work")]
        NotWork
    }
}