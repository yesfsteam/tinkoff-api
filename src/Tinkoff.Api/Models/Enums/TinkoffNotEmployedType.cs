﻿﻿  using System.ComponentModel;

   namespace Tinkoff.Api.Models.Enums
{
    public enum TinkoffNotEmployedType
    {
        [Description("pension_age")]
        PensionAge,
        [Description("pension_disability")]
        PensionDisability,
        [Description("look_work")]
        LookingForWork
    }
}