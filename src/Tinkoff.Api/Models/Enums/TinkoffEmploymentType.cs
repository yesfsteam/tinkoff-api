﻿﻿  namespace Tinkoff.Api.Models.Enums
{
    public enum TinkoffEmploymentType
    {
        NotOfficial = 1,
        Official = 2
    }
}