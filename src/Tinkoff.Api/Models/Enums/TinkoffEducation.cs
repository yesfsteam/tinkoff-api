﻿﻿ namespace Tinkoff.Api.Models.Enums
{
    public enum TinkoffEducation
    {
        None = 0,
        PrimaryHighSchool = 1,
        IncompleteHigherEducation = 2,
        HigherEducation = 3,
        SecondHigherEducations = 4,
        AcademicDegree = 5
    }
}