﻿﻿ namespace Tinkoff.Api.Models.Enums
{
    public enum TinkoffVehicleType
    {
        None = 1,
        Domestic = 1,
        Foreign = 2
    }
}