﻿using System;

namespace Tinkoff.Api.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToTinkoffDateFormat(this DateTime date)
        {
            return date.ToString("ddMMyyyy");
        }
        
        public static string ToTinkoffDateFormat(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("ddMMyyyy") : string.Empty;
        }

    }
}