﻿namespace Tinkoff.Api.Extensions
{
    public static class StringExtensions
    {
        public static string ToTinkoffPhoneFormat(this string phoneNumber)
        {
            return string.IsNullOrWhiteSpace(phoneNumber) ? null : $"8{phoneNumber.Substring(1)}";
        }
    }
}