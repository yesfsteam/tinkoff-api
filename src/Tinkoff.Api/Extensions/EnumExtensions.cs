﻿using System;
using Tinkoff.Api.Models;
using Tinkoff.Api.Models.Enums;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Extensions;

namespace Tinkoff.Api.Extensions
{
    public static class EnumExtensions
    {
        public static string ToTinkoffPhoneType(this AdditionalPhoneNumberType? phoneNumberType)
        {
            if (!phoneNumberType.HasValue)
                return null;
            switch (phoneNumberType.Value)
            {
                case AdditionalPhoneNumberType.Spouse:
                case AdditionalPhoneNumberType.Mother:
                case AdditionalPhoneNumberType.Father:
                case AdditionalPhoneNumberType.Brother:
                case AdditionalPhoneNumberType.Sister:
                case AdditionalPhoneNumberType.Son:
                case AdditionalPhoneNumberType.Daughter:
                case AdditionalPhoneNumberType.Grandmother:
                case AdditionalPhoneNumberType.Grandfather:
                    return ((int)TinkoffPhoneNumberType.Relative).ToString();
                case AdditionalPhoneNumberType.Friend:
                case AdditionalPhoneNumberType.Colleague:
                case AdditionalPhoneNumberType.Other:
                    return ((int)TinkoffPhoneNumberType.Friend).ToString();
                default:
                    throw new ArgumentException($"Invalid {nameof(phoneNumberType)} value", nameof(phoneNumberType));
            }
        } 
        
        public static string ToTinkoffEducation(this Education? education)
        {
            if (!education.HasValue)
                return null;
            switch (education.Value)
            {
                case Education.PrimarySchool:
                case Education.HighSchool:
                case Education.SpecializedHighSchool:
                case Education.IncompleteSecondaryEducation:
                    return ((int)TinkoffEducation.PrimaryHighSchool).ToString();
                case Education.IncompleteHigherEducation:
                    return ((int)TinkoffEducation.IncompleteHigherEducation).ToString();
                case Education.HigherEducation:
                    return ((int)TinkoffEducation.HigherEducation).ToString();
                case Education.TwoOrMoreHigherEducations:
                    return ((int)TinkoffEducation.SecondHigherEducations).ToString();
                case Education.AcademicDegree:
                    return ((int)TinkoffEducation.AcademicDegree).ToString();
                default:
                    throw new ArgumentException($"Invalid {nameof(education)} value", nameof(education));
            }
        }
        
        public static string ToTinkoffActivity(this Activity? activity)
        {
            if (!activity.HasValue)
                return null;
            switch (activity.Value)
            {
                case Activity.Employee:
                    return TinkoffActivity.Work.GetDescription();
                case Activity.SelfEmployed:
                    return TinkoffActivity.Businessman.GetDescription();
                case Activity.Unemployed:
                    return TinkoffActivity.NotWork.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(activity)} value", nameof(activity));
            }
        }
        
        public static string ToTinkoffMaritalStatus(this MaritalStatus? maritalStatus)
        {
            if (!maritalStatus.HasValue)
                return null;
            switch (maritalStatus.Value)
            {
                case MaritalStatus.Single:
                    return ((int)TinkoffMaritalStatus.Single).ToString();
                case MaritalStatus.Married:
                case MaritalStatus.Remarriage:
                    return ((int)TinkoffMaritalStatus.Married).ToString();
                case MaritalStatus.Widowed:
                    return ((int)TinkoffMaritalStatus.Widowed).ToString();
                case MaritalStatus.Divorced:
                    return ((int)TinkoffMaritalStatus.Divorced).ToString();
                case MaritalStatus.CivilMarriage:
                    return ((int)TinkoffMaritalStatus.CivilMarriage).ToString();
                default:
                    throw new ArgumentException($"Invalid {nameof(maritalStatus)} value", nameof(maritalStatus));
            }
        }
        
        public static string ToTinkoffEmployeePosition(this EmployeePosition? employeePosition)
        {
            if (!employeePosition.HasValue)
                return ((int)TinkoffEmployeePosition.None).ToString();
            switch (employeePosition)
            {
                case EmployeePosition.Employee:
                    return ((int)TinkoffEmployeePosition.Employee).ToString();
                case EmployeePosition.Specialist:
                    return ((int)TinkoffEmployeePosition.Specialist).ToString();
                case EmployeePosition.Manager:
                    return ((int)TinkoffEmployeePosition.DivisionHead).ToString();
                case EmployeePosition.Head:
                    return ((int)TinkoffEmployeePosition.Head).ToString();
                default:
                    throw new ArgumentException($"Invalid {nameof(employeePosition)} value", nameof(employeePosition));
            }
        }

        public static string ToTinkoffNotEmployedType(this Activity? activity, DateTime? dateOfBirth)
        {
            if (!activity.HasValue)
                return null;
            switch (activity.Value)
            {
                case Activity.Employee:
                case Activity.SelfEmployed:
                    return string.Empty;
                case Activity.Unemployed:
                    return dateOfBirth.HasValue && (DateTime.Now - dateOfBirth.Value).TotalDays / Constants.DAYS_PER_YEAR >= Constants.PENSION_AGE
                        ? TinkoffNotEmployedType.PensionAge.GetDescription()
                        : TinkoffNotEmployedType.LookingForWork.GetDescription();
                default:
                    throw new ArgumentException($"Invalid {nameof(activity)} value", nameof(activity));
            }
        }
    }
}