﻿using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Tinkoff.Api.Models;

namespace Tinkoff.Api.Data
{
    public interface ICreditApplicationRepository
    {
        Task SaveCreditApplicationRequest(CreditApplicationRequestModel model);
    }

    public class CreditApplicationRepository : ICreditApplicationRepository
    {
        private readonly IConfiguration configuration;

        public CreditApplicationRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task SaveCreditApplicationRequest(CreditApplicationRequestModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.TinkoffResultCode,
                model.TinkoffPayloadId,
                model.TinkoffPayloadErrors,
                model.TinkoffTrackingId,
                model.TinkoffErrorMessage,
                model.TinkoffPlainMessage
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    tinkoff_result_code,
                    tinkoff_payload_id,
                    tinkoff_payload_errors,
                    tinkoff_tracking_id,
                    tinkoff_error_message,
                    tinkoff_plain_message
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :tinkoffResultCode,
                    :tinkoffPayloadId,
                    :tinkoffPayloadErrors,
                    :tinkoffTrackingId,
                    :tinkoffErrorMessage,
                    :tinkoffPlainMessage
                );", queryParams);
        }

        private NpgsqlConnection createConnection()
        {
            return new NpgsqlConnection(configuration.GetConnectionString("Database"));
        }
    }
}