﻿using Dapper;

namespace Tinkoff.Api.Data
{
    public static class DapperInitializer
    {
        public static void ConfigureDapper()
        {
            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }
    }
}