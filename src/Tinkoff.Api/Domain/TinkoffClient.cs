﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Tinkoff.Api.Models;
using Yes.Infrastructure.Http;

namespace Tinkoff.Api.Domain
{
    public interface ITinkoffClient
    {
        Task<Response<TinkoffCreditApplicationResponse>> CreateCreditApplication(Dictionary<string, string> request);
    }

    public class TinkoffClient : ITinkoffClient
    {
        private readonly HttpClient httpClient;

        public TinkoffClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Response<TinkoffCreditApplicationResponse>> CreateCreditApplication(Dictionary<string, string> request)
        {
            return await post<TinkoffCreditApplicationResponse>("v1/add_application", request);
        } 
        
        private async Task<Response<T>> post<T>(string uri, Dictionary<string, string> obj)
        {
            var requestUri = new Uri(httpClient.BaseAddress, uri);
            var content = new FormUrlEncodedContent(obj);
            var response = await httpClient.PostAsync(requestUri, content);
            return await createResponse<T>(response);
        }
        
        private async Task<Response<T>> createResponse<T>(HttpResponseMessage response)
        {
            var result = new Response<T>
            {
                IsSuccessStatusCode = response.IsSuccessStatusCode,
                StatusCode = response.StatusCode
            };
            
            var content = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
                result.Content = JsonConvert.DeserializeObject<T>(content);
            else
                result.ErrorMessage = content;
            
            return result;
        }
    }
}