﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using SD.Messaging.Models;
using Tinkoff.Api.Data;
using Tinkoff.Api.Extensions;
using Tinkoff.Api.Models;
using Tinkoff.Api.Models.Configuration;
using Tinkoff.Api.Models.Enums;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace Tinkoff.Api.Domain
{
    public interface ICreditApplicationManager
    {
        Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event);
       // Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model);
        Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model);
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly ICommandSender commandSender;
        private readonly ITinkoffClient client;
        private readonly ApplicationConfiguration configuration;
        private readonly MessagingConfiguration messagingConfiguration;
        private const string SERVER_UNEXPECTED_ERROR_DETAILS = "Детали ошибки в логах приложения";

        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            ICommandSender commandSender, ITinkoffClient client, ApplicationConfiguration configuration,
            MessagingConfiguration messagingConfiguration)
        {
            this.logger = logger;
            this.repository = repository;
            this.commandSender = commandSender;
            this.client = client;
            this.configuration = configuration;
            this.messagingConfiguration = messagingConfiguration;
        }

        public async Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (@event.ProfileType == ProfileType.Short || @event.ProfileType == ProfileType.Medium)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, "Заявка не отправляется для короткой и средней анкеты");
                    logger.LogInformation($"CheckCreditApplication. Skip. Unsupported profile type. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                
                var request = createRequest(@event, beginTime);
                var response = await client.CreateCreditApplication(request);

                await repository.SaveCreditApplicationRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = @event.CreditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    TinkoffResultCode = response.Content?.resultCode,
                    TinkoffPayloadId = response.Content?.payload?.id,
                    TinkoffPayloadErrors = response.Content?.payload?.errors?.ToJsonString(),
                    TinkoffTrackingId = response.Content?.trackingId,
                    TinkoffErrorMessage = response.Content?.errorMessage,
                    TinkoffPlainMessage = response.Content?.plainMessage
                });

                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content.resultCode == "OK")
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Confirmed, @event.CreditApplicationId);
                        logger.LogInformation($"CheckCreditApplication. Check request approved. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                    }
                    else
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Rejected, @event.CreditApplicationId);
                        logger.LogInformation($"CheckCreditApplication. Check request declined. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                    }
                    return ProcessingResult.Ok();
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogInformation($"CheckCreditApplication. Check request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogError($"CheckCreditApplication. Check request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, @event.CreditApplicationId,
                        $"Количество отправленных запросов: {messagingConfiguration.MaxRetryCount}. Последняя ошибка: {response.ErrorMessage}");
                    logger.LogWarning($"CheckCreditApplication. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                // Переповтор
                logger.LogWarning($"CheckCreditApplication. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}), try again later. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return ProcessingResult.Fail();
            }
            catch (Exception e)
            {
                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, @event.CreditApplicationId,
                        $"Количество попыток обработки: {messagingConfiguration.MaxRetryCount}. {SERVER_UNEXPECTED_ERROR_DETAILS}");
                    logger.LogError(e, $"CheckCreditApplication. Error while processing check request. Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                logger.LogError(e, $"CheckCreditApplication. Error while processing check request, try again later. Duration: {beginTime.GetDuration()} Event: {@event}");
                return ProcessingResult.Fail();
            }
        }
/*
        public async Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            return createResponse(beginTime, CreditOrganizationRequestStatus.Approved);
        }
        */
        public async Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (model.ProfileType == ProfileType.Short || model.ProfileType == ProfileType.Medium)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Skip. Unsupported profile type. Duration: {beginTime.GetDuration()} Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, "Заявка не отправляется для короткой и средней анкеты");
                }
                
                var request = createRequest(model, beginTime);
                var response = await client.CreateCreditApplication(request);

                await repository.SaveCreditApplicationRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = creditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    TinkoffResultCode = response.Content?.resultCode,
                    TinkoffPayloadId = response.Content?.payload?.id,
                    TinkoffPayloadErrors = response.Content?.payload?.errors?.ToJsonString(),
                    TinkoffTrackingId = response.Content?.trackingId,
                    TinkoffErrorMessage = response.Content?.errorMessage,
                    TinkoffPlainMessage = response.Content?.plainMessage
                });

                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content.resultCode == "OK")
                    {
                        logger.LogInformation($"ConfirmCreditApplication. Confirmation request confirmed. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, Response: {response.Content}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Confirmed);
                    }

                    logger.LogInformation($"ConfirmCreditApplication. Confirmation request rejected. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, Response: {response.Content}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.Rejected, response.Content.ToJsonString());
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Confirmation request validation fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, response.ErrorMessage);
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    logger.LogError($"ConfirmCreditApplication. Confirmation request authorization fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, response.ErrorMessage);
                }

                logger.LogWarning($"ConfirmCreditApplication. Confirmation request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, response.ErrorMessage);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"ConfirmCreditApplication. Error while processing confirmation request. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, SERVER_UNEXPECTED_ERROR_DETAILS);
            }
        }

        private Dictionary<string, string> createRequest(CreditApplicationRequest request, DateTime date)
        {
            var workDays = request.EmployeeStartDate.HasValue ? (date - request.EmployeeStartDate.Value).TotalDays : 0;
            var isResidenceAddressEmpty = request.IsResidenceAddressEmpty();
            var result = new Dictionary<string, string>();
            result.Add("app_type_id", "1");
            result.Add("completed", "1");
            result.Add("name", request.FirstName);
            result.Add("surname", request.LastName);
            result.Add("patronymic", request.MiddleName);
            result.Add("phone_mobile", request.PhoneNumber.ToTinkoffPhoneFormat());
            result.Add("email", request.Email);
            result.Add("birthdate", request.DateOfBirth.ToTinkoffDateFormat());
            result.Add("account_duration_months", ((int)(workDays / Constants.DAYS_PER_MONTH)).ToString());
            result.Add("account_duration_years", ((int)(workDays / Constants.DAYS_PER_YEAR)).ToString());
            result.Add("additional_phone_home", request.AdditionalPhoneNumber.ToTinkoffPhoneFormat());
            result.Add("additional_phone_home_type", request.AdditionalPhoneNumberType.ToTinkoffPhoneType());
            result.Add("additional_phone_home_comment", request.AdditionalPhoneNumberType.GetDescription());
            result.Add("addresstype_home_area", isResidenceAddressEmpty ?
                request.RegistrationAddressCity : 
                request.ResidenceAddressCity);
            result.Add("addresstype_home_building", isResidenceAddressEmpty ? 
                request.RegistrationAddressHouse :
                request.ResidenceAddressHouse);
            result.Add("addresstype_home_city", isResidenceAddressEmpty ? 
                request.RegistrationAddressCity :
                request.ResidenceAddressCity);
            result.Add("addresstype_home_corpus", isResidenceAddressEmpty ? 
                request.RegistrationAddressBlock :
                request.ResidenceAddressBlock);
            result.Add("addresstype_home_flat", isResidenceAddressEmpty ? 
                request.RegistrationAddressApartment :
                request.ResidenceAddressApartment);
            result.Add("addresstype_home_place", isResidenceAddressEmpty ? 
                request.RegistrationAddressRegion :
                request.ResidenceAddressRegion);
            //result.Add("addresstype_home_postal_code", string.Empty);
            result.Add("addresstype_home_street", isResidenceAddressEmpty ? 
                request.RegistrationAddressStreet :
                request.ResidenceAddressStreet);
            result.Add("addresstype_home_stroenie", isResidenceAddressEmpty ? 
                request.RegistrationAddressBuilding :
                request.ResidenceAddressBuilding);
            result.Add("addresstype_registered_area", request.RegistrationAddressCity);
            result.Add("addresstype_registered_building", request.RegistrationAddressHouse);
            result.Add("addresstype_registered_city", request.RegistrationAddressCity);
            result.Add("addresstype_registered_corpus", request.RegistrationAddressBlock);
            result.Add("addresstype_registered_flat", request.RegistrationAddressApartment);
            result.Add("addresstype_registered_place", request.RegistrationAddressRegion);
            //result.Add("addresstype_registered_postal_code", string.Empty);
            result.Add("addresstype_registered_street", request.RegistrationAddressStreet);
            result.Add("addresstype_registered_stroenie", request.RegistrationAddressBuilding);
            result.Add("addresstype_work_area", request.EmployerAddressRegion);
            result.Add("addresstype_work_building", request.EmployerAddressHouse);
            result.Add("addresstype_work_city", request.EmployerAddressCity);
            result.Add("addresstype_work_corpus", request.EmployerAddressBlock);
            result.Add("addresstype_work_flat", request.EmployerAddressApartment);
            result.Add("addresstype_work_place", request.EmployerAddressRegion);
            //result.Add("addresstype_work_postal_code", string.Empty);
            result.Add("addresstype_work_street", request.EmployerAddressStreet);
            result.Add("addresstype_work_stroenie", request.EmployerAddressBuilding);
            result.Add("asset_foreign_vehicle_flag", ((int)(request.HasCar ? TinkoffVehicleType.Foreign : TinkoffVehicleType.None)).ToString());
            result.Add("education", request.Education.ToTinkoffEducation());
            result.Add("employment_type", request.Activity.ToTinkoffActivity());
            //result.Add("expenses_amount", string.Empty);
            result.Add("income_individual", request.MonthlyIncome?.ToString());
            result.Add("marital_status", request.MaritalStatus.ToTinkoffMaritalStatus());
            result.Add("not_official", ((int)TinkoffEmploymentType.Official).ToString());
            result.Add("not_work", request.Activity.ToTinkoffNotEmployedType(request.DateOfBirth));
            //result.Add("not_work_other_text", string.Empty);
            result.Add("work_name", request.EmployerName);
            result.Add("work_position", request.EmployeePosition.ToTinkoffEmployeePosition());
            result.Add("work_position_text", request.EmployeePosition.GetDescription());
            result.Add("id_division_code", request.PassportDepartmentCode);
            result.Add("passport_number", request.PassportNumber);
            result.Add("passport_series", request.PassportSeries);
            result.Add("passport_who_given", request.PassportIssuer);
            result.Add("passport_date_given", request.PassportIssueDate.ToTinkoffDateFormat());
            result.Add("phone_work", request.EmployerPhoneNumber.ToTinkoffPhoneFormat());
            //result.Add("phone_work_ext", string.Empty);
            result.Add("place_of_birth", request.PlaceOfBirth);
            result.Add("utm_campaign", "api");
            result.Add("wm", "api");
            result.Add("channel", "Internet");
            result.Add("utm_medium", "dsp.apr");
            result.Add("utm_source", "yesfs_cc");
            //result.Add("has_foreign_pass", string.Empty);
            //result.Add("show_foreign_passport", string.Empty);
            //result.Add("visit_foreign_country_frequency", string.Empty);
            //result.Add("salary_bank", string.Empty);
            result.Add("tid", "8c0fafdc66083c08dd2f1aaf7950a009");
            result.Add("product_category", "Credit Cards");
            result.Add("product_name", "Tinkoff Platinum");
            //result.Add("desired_credit_limit ", string.Empty);
            //result.Add("card_want_reason ", string.Empty);
            
            return result;
        }
        
        private CreditApplicationDecisionsCommand publishDecisionCommand(DateTime date, CreditOrganizationRequestStatus status, Guid creditApplicationId, string details = null)
        {
            var command = new CreditApplicationDecisionsCommand
            {
                Date = date,
                CreditApplicationId = creditApplicationId,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }
        
        private Response<CreditApplicationResponse> createResponse(DateTime date, CreditOrganizationRequestStatus status, string details = null)
        {
            return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
            {
                Date = date,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            });
        }
    }
}